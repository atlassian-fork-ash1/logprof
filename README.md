LogProf
=======

A simple profiling tool based on analyzing the speed at which logs are received.

If you have a long running process (e.g. a build) that emits logs, this tool lets you find where your time went by highlighting which bits are slow, which is the first step to speeding up that long running process.

`logprof` works in 2 modes:

1. recording mode: pipe lines to `logprof` and have it emit those lines with timestamps prepended.
2. reporting mode: pipe lines with timestamps to `logprof` and have it emit a display of "how long each line took" before the next line was recorded.

`logprof record` is intended to be piped to `logprof report`, but you should be able to use `logprof report` with any file that has timestamps on each line (see `logprof --help`).

General usage
=============

    cargo build --release
    <something like mvn install> | target/release/logprof record | target/release/logprof report

Concrete example
================

We have a Maven build (i.e. a Java build) which I feel is slow - but as it runs, I just see a bunch of text scrolling past; which bits are actually slow?

I know that when a Maven build runs, all the actual building is done by Maven "plugins", so I know it must be that some of the plugins are slow; if I knew which ones, I could try to optimize them or perhaps remove them entirely. I know plugin executions start with '--- ' followed by their name, so I run `mvn clean install -DskipTests | grep -- '--- ' | logprof record > samples/mvn-user-management` to get a build log of all the plugins.

It takes about a minute - where did all that time go?

```
$ cat samples/mvn-user-management | tail -n 20
2016-03-10T08:49:07.540394077Z [INFO] --- maven-compiler-plugin:2.5:compile (default-compile) @ user-provisioning-acceptance-tests ---
2016-03-10T08:49:07.540408403Z [INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ user-provisioning-acceptance-tests ---
2016-03-10T08:49:07.542089349Z [INFO] --- maven-compiler-plugin:2.5:testCompile (default-testCompile) @ user-provisioning-acceptance-tests ---
2016-03-10T08:49:07.841004766Z [INFO] --- maven-surefire-plugin:2.17:test (default-test) @ user-provisioning-acceptance-tests ---
2016-03-10T08:49:07.875250880Z [INFO] --- maven-jar-plugin:2.3.2:jar (default-jar) @ user-provisioning-acceptance-tests ---
2016-03-10T08:49:07.958506139Z [INFO] --- maven-amps-plugin:5.1.20:integration-test (integration-test) @ user-provisioning-acceptance-tests ---
2016-03-10T08:49:07.961893743Z [INFO] --- maven-install-plugin:2.5.1:install (default-install) @ user-provisioning-acceptance-tests ---
2016-03-10T08:49:07.977498740Z [INFO] --- maven-clean-plugin:2.6.1:clean (default-clean) @ user-provisioning-webdriver-tests ---
2016-03-10T08:49:07.979284105Z [INFO] --- maven-enforcer-plugin:1.3.1:enforce (enforce-build-environment) @ user-provisioning-webdriver-tests ---
2016-03-10T08:49:07.983391152Z [INFO] --- maven-enforcer-plugin:1.3.1:enforce (ban-milestones-and-release-candidates) @ user-provisioning-webdriver-tests ---
2016-03-10T08:49:07.996063098Z [INFO] --- maven-enforcer-plugin:1.3.1:enforce (no-old-hamcrest-versions) @ user-provisioning-webdriver-tests ---
2016-03-10T08:49:08.008577195Z [INFO] --- maven-resources-plugin:2.6:copy-resources (copy-license) @ user-provisioning-webdriver-tests ---
2016-03-10T08:49:08.011318532Z [INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ user-provisioning-webdriver-tests ---
2016-03-10T08:49:08.012594656Z [INFO] --- maven-compiler-plugin:2.5:compile (default-compile) @ user-provisioning-webdriver-tests ---
2016-03-10T08:49:08.065585971Z [INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ user-provisioning-webdriver-tests ---
2016-03-10T08:49:08.067079709Z [INFO] --- maven-compiler-plugin:2.5:testCompile (default-testCompile) @ user-provisioning-webdriver-tests ---
2016-03-10T08:49:08.118403566Z [INFO] --- maven-surefire-plugin:2.17:test (default-test) @ user-provisioning-webdriver-tests ---
2016-03-10T08:49:08.120086173Z [INFO] --- maven-jar-plugin:2.3.2:jar (default-jar) @ user-provisioning-webdriver-tests ---
2016-03-10T08:49:08.124362359Z [INFO] --- maven-jar-plugin:2.3.2:test-jar (default) @ user-provisioning-webdriver-tests ---
2016-03-10T08:49:08.128072663Z [INFO] --- maven-install-plugin:2.5.1:install (default-install) @ user-provisioning-webdriver-tests ---
```

The `logprof record` has added the timestamps at the start of each line; we can see that none of the plugins took that long but it's also a pain to scan through the entire output to find which plugins took longest.

**Wouldn't it be nice if we could sort the lines by the time difference between them?** If we use `logprof` together with the standard `sort`, we can!

```
$ logprof report --graph-type sqrt | sort -n | tail -n 20 < samples/mvn-user-management
00:00:00.602 ██-------- [INFO] --- lesscss-maven-plugin:3.0.1:compile (compile-user-provisioning-plugin-less) @ user-provisioning-plugin ---
00:00:00.676 ██-------- [INFO] --- maven-dependency-plugin:2.8:unpack (unpack) @ horde-web-app ---
00:00:00.727 ██-------- [INFO] --- gmavenplus-plugin:1.5:execute (default) @ crowd-user-management ---
00:00:00.760 ██-------- [INFO] --- exec-maven-plugin:1.4.0:exec (compile-soy) @ unified-admin-chrome ---
00:00:00.805 ██-------- [INFO] --- maven-compiler-plugin:2.5:testCompile (default-testCompile) @ user-provisioning-plugin ---
00:00:00.862 ██-------- [INFO] --- maven-install-plugin:2.5.1:install (default-install) @ user-management-freezer-slats ---
00:00:01.043 ██-------- [INFO] --- lesscss-maven-plugin:3.0.1:compile (compile-unified-admin-chrome-less) @ unified-admin-chrome ---
00:00:01.305 ███------- [INFO] --- maven-shade-plugin:2.2:shade (default) @ horde-tenant-initialiser ---
00:00:01.323 ███------- [INFO] --- maven-amps-plugin:5.1.20:jar (default-jar) @ user-provisioning-plugin ---
00:00:01.392 ███------- [INFO] --- maven-compiler-plugin:2.5:compile (default-compile) @ user-management-client-common ---
00:00:01.406 ███------- [INFO] --- git-commit-id-plugin:2.1.14:revision (default) @ user-management-distribution-standalone ---
00:00:02.022 ███------- [INFO] --- maven-compiler-plugin:2.5:compile (default-compile) @ user-provisioning-plugin ---
00:00:02.482 ████------ [INFO] --- maven-compiler-plugin:2.5:testCompile (default-testCompile) @ crowd-user-management-plugin ---
00:00:02.488 ████------ [INFO] --- maven-amps-plugin:5.1.20:generate-manifest (default-generate-manifest) @ user-provisioning-plugin ---
00:00:02.532 ████------ [INFO] --- maven-compiler-plugin:2.5:compile (default-compile) @ crowd-user-management-plugin ---
00:00:02.773 ████------ [INFO] --- maven-war-plugin:2.1.1:war (default-war) @ horde-web-app ---
00:00:04.811 █████----- [INFO] --- exec-maven-plugin:1.4.0:exec (npm-install) @ crowd-user-management-plugin ---
00:00:10.059 ████████-- [INFO] --- maven-amps-plugin:5.1.20:copy-bundled-dependencies (default-copy-bundled-dependencies) @ user-provisioning-plugin ---
00:00:13.715 ██████████ [INFO] --- exec-maven-plugin:1.4.0:exec (generate-um-soy-and-js) @ crowd-user-management-plugin ---
Min = 00:00:00.000, max = 00:00:13.715, total = 00:01:07.973, graph type = square root
```

At the bottom we can see that `exec-maven-plugin:1.4.0:exec (generate-um-soy-and-js) @ crowd-user-management-plugin` took a whole 13.7 seconds until the next plugin execution ran, and the plugins above it are sorted according to the time they took to run as well. Some tips:

* the ASCII graph here displays how long each line took relative to how long the longest line took. The `--graph-type sqrt` makes it display as a Square Root graph, which generally provides a nice mix of showing relative times while still drawing attention to somewhat-fast-but-still-slow lines.
  * the default is a `linear` graph because that's hardest to misinterpret, but it tends to make one ignore those "slightly slow" lines (sometimes resulting in "death by a thousand cuts").
  * `log` graph is another option, most useful when you have a single line that takes a lot of time relative to others.
  * experiment to find what works best for your dataset!
* we could have left off the `| sort -n` (try it!) if we wanted to visually scan through the output instead; that can be more useful if your interesting log lines don't include enough context on their own (here we have the plugin execution name and the module of the build, which is enough for me to figure out where this plugin execution is defined).

More usages
===========

Remember, normally you'd pipe some log text to `logprof record` to make a recording then to `logprof report`. But to get started, this repo comes with some sample recordings in `samples/`.

So here are a bunch more examples:

```
# show recording and reporting with manual input
logprof record | logprof report

# see the time it takes the compile logprof and each of its dependencies
logprof report samples/cargo-logprof-release

# see maven plugin executions which take at least 10% of the time taken by the slowest plugin execution
# (--display-min 1 is saying "display only items which show at least 1 bar in the graph")
logprof report --display-min 1 < samples/mvn-user-management

# see maven plugin executions on a sqrt graph
logprof report --graph-type sqrt < samples/mvn-user-management

# sort by maven plugin execution times and show top 20 slowest plugin executions (aka detailed example earlier)
logprof report --graph-type sqrt < samples/mvn-user-management | sort -n | tail -n 20

# see task timings in a Bamboo build job in a square root graph (raw input)
logprof report --time-format '%d-%b-%Y %H:%M:%S' \
    --parse-with '^\w+\s+(?P<time>\d\d-\w{3}-\d{4} \d\d:\d\d:\d\d)\s+?(?P<log>.*)' \
    --graph-type sqrt \
    < samples/bamboo-job-user-management-webdriver

# see detailed help
logprof --help
```

Development
===========

Install Rustup and Rust (stable) using https://www.rustup.rs/ .

Get feedback from type errors in a terminal:

    cargo install cargo-check # installs cargo check for faster compilation feedback (skips codegen)
    cargo install cargo-watch # installs cargo watch to run cargo commands on file changes
    cargo watch check         # run "cargo check" each time a file change is detected

Build and run the app in another terminal:

    cargo build && env RUST_BACKTRACE=1 RUST_LOG=debug ./target/debug/logprof
