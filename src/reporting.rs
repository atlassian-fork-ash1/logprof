use chrono::{Duration, TimeZone, UTC};
use regex::Regex;
use std::io::{self, BufRead, BufWriter, Write};

pub const TIME_CAPTURE_NAME: &'static str = "time";
pub const LOG_CAPTURE_NAME: &'static str = "log";

pub const MAX_GRAPH_WIDTH: u8 = 10;

const GRAPH_BODY_STR: &'static str = "\u{2588}"; // unicode full block character

#[derive(Debug, RustcDecodable, Clone, Copy)]
pub enum GraphType { Linear, Sqrt, Log }

#[derive(Debug)]
pub struct ReportOptions {
    pub time_format: String,
    pub parser_regex: Regex,
    pub display_min: u8,
    pub graph_type: GraphType,
}

pub fn report<R: BufRead, W: Write>(input: &mut R, output: &mut BufWriter<W>, options: &ReportOptions) -> io::Result<()> {
    debug!("Reporting with options={:#?}", options);

    let report = try!(build_report(input, options.time_format.as_ref(), &options.parser_regex));
    display_report(output, &report, &options)
}

#[derive(Debug)]
struct ReportLine {
    duration: Duration,
    log: String,
}

fn build_report<R: BufRead>(input: &mut R, time_format: &str, parser_regex: &Regex) -> io::Result<Vec<ReportLine>> {
    let mut report = Vec::new();

    let mut prev_line = None;
    for line_result in input.lines() {
        let line = try!(line_result);
        if let Some(captures) = parser_regex.captures(line.as_str()) {
            let timestamp_str = captures.name(TIME_CAPTURE_NAME)
                .expect("regex always has TIME_CAPTURE_NAME");
            let log_str = captures.name(LOG_CAPTURE_NAME)
                .expect("regex always has LOG_CAPTURE_NAME");

            let timestamp = try!(UTC.datetime_from_str(timestamp_str, time_format)
                .map_err(|err| io::Error::new(io::ErrorKind::InvalidInput,
                        format!("Failed to parse timestamp of '{}' with format of '{}': {}",
                            timestamp_str, time_format, err))
                ));

            if let Some((prev_timestamp, prev_log)) = prev_line {
                report.push(ReportLine {
                    duration: timestamp - prev_timestamp,
                    log: prev_log,
                })
            };
            prev_line = Some((timestamp, log_str.to_string()));
        } else {
            debug!("parser_regex failed on line: {}", line);
        }
    };
    Ok(report)
}

fn display_report<W: Write>(output: &mut BufWriter<W>, report: &Vec<ReportLine>, options: &ReportOptions) -> io::Result<()> {
    let graph_str_byte_size = GRAPH_BODY_STR.as_bytes().len();
    let graph_text = (0..MAX_GRAPH_WIDTH).map(|_| GRAPH_BODY_STR).collect::<String>();

    for max_duration in report.iter().map(|l| l.duration).max() { // aka "if non-empty"
        let max_duration_ms = if max_duration.num_milliseconds() == 0 {
            debug!("max_duration_ms=0, i.e. equal time lines; setting max_duration_ms=1");
            1.0
        } else {
            debug!("max_duration_ms={}", max_duration.num_milliseconds());
            max_duration.num_milliseconds() as f64
        };

        for line in report.iter() {
            let line_duration = line.duration.num_milliseconds() as f64;
            let proportion: f64 = if line_duration == 0.0 {
                0.0
            } else {
                match options.graph_type {
                    GraphType::Linear => line_duration / max_duration_ms,
                    GraphType::Log => line_duration.log2() / max_duration_ms.log2(),
                    GraphType::Sqrt => line_duration.sqrt() / max_duration_ms.sqrt(),
                }
            };
            assert!(proportion >= 0.0, "proportion={} >= 0.0", proportion);
            assert!(proportion <= 1.0, "proportion={} <= 1.0", proportion);
            let relative_time = (MAX_GRAPH_WIDTH as f64 * proportion) as usize;
            if relative_time >= options.display_min as usize {
                let graph = &graph_text.as_str()[0..relative_time * graph_str_byte_size];
                let time = format_duration(&line.duration);
                try!(writeln!(output, "{time} {graph:-<10} {log}", graph = graph, time = time,
                        log = line.log));
            }
        }

        let total_duration = report.iter().fold(Duration::zero(), |d, l| d + l.duration);
        let min_duration = report.iter().map(|l| l.duration).min().expect("len >= 1");
        try!(writeln!(output, "Min = {min}, max = {max}, total = {total}, graph type = {graph_type}",
            min = format_duration(&min_duration),
            max = format_duration(&max_duration),
            total = format_duration(&total_duration),
            graph_type = match options.graph_type {
                    GraphType::Linear => "linear",
                    GraphType::Log => "logarithmic, base 2",
                    GraphType::Sqrt => "square root",
                },
            ));
    }
    Ok(())
}

fn format_duration(duration: &Duration) -> String {
    format!("{hours:>02}:{minutes:>02}:{seconds:>02}.{ms:>03}",
        hours = duration.num_hours() % 24,
        minutes = duration.num_minutes() % 60,
        seconds = duration.num_seconds() % 60,
        ms = duration.num_milliseconds() % 1000,
    ).to_string()
}
